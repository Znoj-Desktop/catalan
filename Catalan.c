#include <stdio.h>
#include <stdlib.h>
const int N = 16;

int M_Rec = 0;
int M_D = 0;

int CatalanR(int n){
	if(n == 0){
		return 1;
	}
	int sum = 0;
	int i;
	for(i=0; i<n; i++){
		M_Rec++;
		sum += (CatalanR(i)*CatalanR(n-i-1));
	}
	return sum;
}

int CatalanD(int n){
	int i = 0;
	int j = 0;	
	int array[100+1];
	for(i=0; i<n+1; i++){
		array[i] = 0;	
	}
	array[0] = 1;
	for(i=1; i<n+1; i++){
		for(j=0; j<i; j++){
			M_D++;
			array[i]+=array[j]*array[i-j-1];
		}
	}
	return array[n];
}


int main(){
	int i;	
	int c;
	for(i=0; i<N; i++){
		M_Rec = 0;
		c = CatalanR(i);
		printf("%d: %d...%d\n", i, c, M_Rec);
	}

	printf("==============\n");

	for(i=0; i<N; i++){
		M_D = 0;
		c = CatalanD(i);
		printf("%d: %d...%d\n", i, c, M_D);
	}
	return 0;
}
